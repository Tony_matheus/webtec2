import React, {Component, Fragment} from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPhone} from "@fortawesome/free-solid-svg-icons/faPhone";
import {NavbarMain} from './components/Navbar'
import MainApresentation from './components/Main';
import Footer from './components/Footer'
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";

library.add(faPhone, faEnvelope);

class App extends Component {
  render() {
    return (
        <Fragment>
          <NavbarMain/>
          <MainApresentation/>
          <Footer/>
        </Fragment>
    );
  }
}

export default App;
