import styled from 'styled-components';

const Text = styled.p`
  width: 100%;
  height: 12px;
  font-family: Poppins;
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 16px;
  letter-spacing: normal;
  text-align: left;
  color: #4d4d4d;
  padding-left:13px;
`;

export default Text;