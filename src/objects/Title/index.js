import styled  from 'styled-components';

const Title = styled.h2`
  margin-left: 6%;
  margin-right: auto;
  width: 152px;
  height: 36px;
  font-family: Poppins;
  font-size: 36px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 66px;
  letter-spacing: normal;
  text-align: left;
  color: var(--color-title);
`;

export default Title;