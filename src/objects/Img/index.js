import React from 'react';
import styled from 'styled-components';
import Image_child from '../../imgs/Image_child.png';

const Img = () =>(
    <img alt="Smiley face"  src={Image_child}/>
);

export default Img;