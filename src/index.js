import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route} from "react-router-dom";

import Global from './styles/settings';
import Base from './styles/base';
import Reset from './styles/generic';

import App from './App';
import Employee from "./Employees";
ReactDOM.render(
    <Fragment>
        <Global.Colors/>
        <Base/>
        <Reset/>
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={App}/>
                <Route exact path='/t' component={Employee}/>
                {/* both /roster and /roster/:number begin with /roster */}
                {/*<Route path='/roster' component={Roster}/>*/}
                {/*<Route path='/schedule' component={Schedule}/>*/}
            </Switch>
        </BrowserRouter>
    </Fragment>,
    document.getElementById('root'));
