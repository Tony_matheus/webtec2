import React, {Component, Fragment} from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {NavbarMain} from './components/Navbar'
import Footer from './components/Footer'
import CardMain from "./components/CardMain";
import RecipeReviewCard from "./components/CardMain2";
import ListBar from './components/ListBar';


class Employee extends Component {
    render() {
        return (
            <Fragment>
                <NavbarMain/>
                <ListBar/>
                <Footer/>
            </Fragment>
        );
    }
}

export default Employee;
