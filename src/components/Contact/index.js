import React, {Fragment, Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Contact from './style';
import Content from '../../objects/Content';
import Text from '../../objects/Text'

class ContactMain extends Component {
    state = {
        datas: [
            {
                icon: "phone",
                text: "+880 1971 502654"
            },{
                icon: "envelope",
                text: "MehediHas1994@gmail.com"
            }
        ]
    };

    render(){
        return(
            <Fragment>
                <Contact>
                {this.state.datas.map(({icon, text}, index)=> {
                    return (
                        <Content key={index}>
                            <FontAwesomeIcon icon={icon} />
                            <Text>{text}</Text>
                        </Content>
                    )
                })}
                </Contact>
            </Fragment>
        )
    }
}

export default ContactMain;
