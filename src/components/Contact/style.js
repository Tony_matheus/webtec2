import styled from 'styled-components';

const Contact = styled.div`
  display: flex;
  padding: 0;
  float: right;
  margin: 0 5% 0 0;
  width: 50%;
  color: #fff;
`;

export default Contact;
