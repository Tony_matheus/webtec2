import styled from "styled-components";

const NavLink = styled.div`
  padding: 0;
  float: right;
  margin: 10px 5% 0 0;
  width: 35%;
`;

export default NavLink;