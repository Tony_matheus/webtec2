import {Component, Fragment} from "react";
import Content from "../index";
import {Link} from "react-router-dom";
import React from "react";
import NavLink from './style';
import Button from '../../Button';

class NavLinks extends Component{
    state = {
        datas: [
            {
                text: "About",
                link: '/t'
            },{
                text: "Home",
                link: '/'
            }
        ]
    };

    render(){
        return(
            <Fragment>
                <NavLink>
                    {this.state.datas.map(({text, link}, index)=> {
                        return (
                            <Link to={link}>
                                <Button key={index} >
                                    {text}
                                </Button>
                            </Link>
                        )
                    })}
                </NavLink>
            </Fragment>
        )
    }
}

export default NavLinks;