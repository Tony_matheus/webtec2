import styled from 'styled-components';

const Navbar = styled.header` 
    width: 100%;
    height: 86px;
    display: flex;
    margin-top: 0;
`;

export default Navbar;
