import React , {Component, Fragment} from 'react';
import {Link} from 'react-router-dom'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import Button from '../Button';
import Navbar from './style';
import ContactMain from '../Contact'
import Title from '../../objects/Title'
import Content from '../../objects/Content';
import NavLinks from './NavLinks';
class NavbarMain extends Component{
    render(){
        return (
            <Fragment>
                <Navbar>
                    <Fragment>
                        <Title>Childcare</Title>
                        <NavLinks/>
                    </Fragment>
                </Navbar>
            </Fragment>
        )
    }
}

export {NavbarMain};
