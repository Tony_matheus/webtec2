import styled from 'styled-components';

const FooterStyle = styled.footer` 
  width: 100%;
  height: 46px;
  background-image: linear-gradient(80deg, #f4627c, #b539a8);
  
  position:fixed; 
  bottom:0; 
`;

export default FooterStyle;
