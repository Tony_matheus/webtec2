import React , {Component, Fragment} from 'react';

import { Main, Text, TextTitle, ContainerText, ContainerImg, Box} from './style';

import Img from '../../objects/Img';

export default class MainApresentation extends Component {
  render() {
      return(
          <Fragment>
              <Main>
                  <ContainerText>
                      <Box>
                          <div>
                              <TextTitle>A Child is An Uncut Diamond</TextTitle>
                              <Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                          </div>
                      </Box>
                  </ContainerText>
                  <ContainerImg>
                    <Img style={"float:right"}/>
                  </ContainerImg>
              </Main>
          </Fragment>
      )
  }
}