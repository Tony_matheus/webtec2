import styled from 'styled-components';

const Main = styled.main`
    display: inline-flex;
    width: 100%;
`;

const ContainerText = styled.div`
    display:block;
    padding: 10px;
    width: 40%;
`;

const ContainerImg = styled.div`
    padding: 10px;
    width: 60%;
    margin-right: 0;
`;

const TextTitle = styled.h5`
      width: 64%;
    height: 41px;
    font-family: Poppins;
    font-size: 33px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 33px;
    -webkit-letter-spacing: normal;
    -moz-letter-spacing: normal;
    -ms-letter-spacing: normal;
    letter-spacing: normal;
    text-align: left;
    padding-bottom: 100px;
    color: #0f3553;
    margin-left: auto;
    margin-right: auto;
`;

const Text = styled.p`
    width: 64%;
    height: 41px;
    font-family: Poppins;
    font-size: 18px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.22;
    letter-spacing: normal;
    text-align: left;
    color: #4d4d4d;
    margin-left: auto;
    margin-right: auto;
`;

const Box = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 400px;
`

export { Main, Text, TextTitle, ContainerText, ContainerImg, Box};