import styled from 'styled-components';

const Button = styled.button`
  width: 110.5px;
  height: 41.4px;
  border-radius: 7px;
  cursor: pointer;
  color: #fff;
  transition: all 200ms linear;
  float: right;
  margin-right: 10%;

  background-image: linear-gradient(81deg, var(--color-start), var(--color-medium));
  
  &:hover {
    color: var(--color-start);
    background-color: transparent;
    background-image: none;
    border-color: var(--color-start);
    >a{
        text-decoration: none;
        color:  var(--color-start);
      }
  }
  
  &:active{
    color: var(--color-start);
    background-color: transparent;
    background-image: none;
    border-color: var(--color-start);
    
      >a{
        text-decoration: none;
        color:  var(--color-start);
      }
  }

  >a{
    text-decoration: none;
    color: #fff;
  }
`

export default Button;