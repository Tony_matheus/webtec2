import styled from 'styled-components';

const BarStyle = styled.div`
    z-index: 1050;
    margin-top: 30px;
    margin-left: 30px;
    display: inline-block;
    padding: 28px;
    align-items: center;
    width: 400px;
    //border: 1px solid black;
    border-radius: 10px;
    box-shadow: 0px 0px 21px 0px rgba(181,177,181,1);
 `;

const Information = styled.div`
  padding-top: 10px;
`;

export default BarStyle;
export {Information};