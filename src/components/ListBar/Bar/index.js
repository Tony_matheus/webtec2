import React, {Fragment, Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import BarStyle, { Information } from './style';
import Text from '../../../objects/Text'
import Collapse from '@material-ui/core/Collapse';
import Button from '../../Button'
import {ContainerText} from '../../Main/style';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Position from '../../../objects/Position';
import Email from '../../../objects/Email';

import ImgWoman from '../../../imgs/woman_avatar.png';
import ImgMan from '../../../imgs/man_avatar.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class Bar extends Component{
    state = {
        expand: false,
        woman: true
    };

    handleExpandClick = () => {
        this.setState(state => ({ expand: !state.expand }));
    };

    render(){
        return(
            <BarStyle onClick={this.handleExpandClick}>
                <Row>
                    <Col sm={3}>
                        <Avatar src={(this.props.woman) ? ImgWoman : ImgMan }/>
                    </Col>
                    <Col>
                        <h5>{this.props.name}</h5>
                    </Col>
                </Row>
                <Collapse in={this.state.expand} timeout="auto" unmountOnExit>
                    <Information>
                        <Row>
                            <Col>
                                <Position>{this.props.position}</Position>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Email> <FontAwesomeIcon icon="envelope-open" />{this.props.email}</Email>
                            </Col>
                            <Col>
                                <Email>{this.props.phone}</Email>
                            </Col>
                        </Row>
                    </Information>
                </Collapse>
            </BarStyle>
        )
    }
}