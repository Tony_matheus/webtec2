import styled from 'styled-components';

const ListBarStyle = styled.div`
  margin-right: auto;
  margin-left: auto;
  width: 100%;
  height: 100px;
`;

export default ListBarStyle;