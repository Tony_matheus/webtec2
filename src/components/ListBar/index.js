import React, {Fragment, Component} from 'react';
import ListBarStyle from './style';
import Bar from './Bar';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default class ListBar extends Component{

    state = {
        employeesW: [
            {
                name:"Ariel Nicodemos",
                email: "ariel@childcare.com",
                position: "Designer",
                phone: "(12) 94567-6543",
                woman: true
            },{
                name:"Thais Palha",
                email: "thais@childcare.com",
                position: "Professora",
                phone: "(12) 94567-6543",
                woman: true
            },{
                name:"Manuela Versosa",
                email: "thais@childcare.com",
                position: "Engenheira Front-end",
                phone: "(12) 94567-6543",
                woman: true
            }
        ],
        employeesM: [
            {
                name:"Tony Matheus",
                email: "tony@childcare.com",
                position: "Developer",
                phone: "(12) 94567-6543",
                woman: false
            },{
                name:"Marcio Heleno",
                email: "marcio@childcare.com",
                position: "Developer",
                phone: "(12) 94567-6543",
                woman: false
            },{
                name:"Sergio",
                email: "marcio@childcare.com",
                position: "Engenheiro Front-end",
                phone: "(12) 94567-6543",
                woman: true
            }
        ]
    };

    render(){
        return(
            <Fragment>
                <ListBarStyle>
                    <Row>
                        <Col>
                            <Row>{this.state.employeesW.map(employee => (
                                    <Col>
                                        <Bar {...employee}/>
                                    </Col>
                                ))}</Row>
                        </Col>
                        <Col>
                            <Row>
                                {this.state.employeesM.map(employee => (
                                    <Col>
                                        <Bar {...employee}/>
                                    </Col>
                                ))}
                            </Row>
                        </Col>
                    </Row>
                </ListBarStyle>
            </Fragment>
        )
    }
}