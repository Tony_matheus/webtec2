import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle `
  :root{
    --color-start:#f4627c;
    --color-medium: #b539a8;
    --color-end: #b539a8;
    --color-title: #e35787;
    --color-zero: #fff;
    --gradient-color: linear-gradient(83deg, #f4627c, #b539a8 96%, #b539a8);  
    //--gradient-color: linear-gradient(83deg, #f4627c, #b539a8 96%, #b539a8);  
  }
`;

export default GlobalStyles;