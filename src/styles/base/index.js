import {createGlobalStyle} from "styled-components";

const Base = createGlobalStyle`
  body{
    font-family: Poppins, sans-serif;;
  }     
`;

export default Base;